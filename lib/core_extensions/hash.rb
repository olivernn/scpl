class Hash
  def only(*keys)
    self.reject { |k,v| !keys.include?(k) }
  end
end
