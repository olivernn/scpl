define(['jquery'], function ($) {
  // small jquery plugin to serialize a form to key valu pairs in an object
  // makes it easier to update models from a form
  return $.fn.serializeObject = function () {
    var form = this

    return this.serializeArray().reduce(function (memo, pair) {
      memo[pair.name] = pair.value
      return memo
    }, {})
  }
})
