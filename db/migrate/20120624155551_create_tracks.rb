class CreateTracks < ActiveRecord::Migration
  def change
    create_table :tracks do |t|
      t.integer :playlist_id
      t.integer :sc_id
      t.string :title
      t.timestamps
    end
  end
end
