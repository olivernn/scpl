class Playlist < ActiveRecord::Base
  attr_accessible :title, :description

  has_many :tracks do
    def create_from_url(url)
      attrs = SC.get('/resolve', url: url)
      proxy_association.owner.tracks.create sc_id: attrs["id"], title: attrs["title"]
    end
  end
end
