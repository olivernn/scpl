class Track < ActiveRecord::Base
  attr_accessible :title, :sc_id

  belongs_to :playlist
end
