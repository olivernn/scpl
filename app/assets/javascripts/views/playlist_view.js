define([
  'jquery',
  'backbone',
  'underscore',
  'template!playlist',
  'collections/track_search_results',
  'views/track_search_results_view',
  'views/tracks_view',
  'jquery_plugins/serialize_object'
], function ($, backbone, _, template, TrackSearchResults, TrackSearchResultsView, TracksView) {

  // autamagically bind to attributes to relevant parts of the view markup.
  // looks for data-bind attributes and sets up bindings to the relevant model attribtues.
  //
  // <input type="text" data-bind-val="title"></input>
  //
  // The above would change the value of the input to the value of the title attribute
  // on the model everytime a change:title event is triggered
  var autoBind = function (el, model, type) {
    el.find('[data-bind-' + type + ']').each(function () {
      var el = $(this),
          attrName = el.data('bind-' + type),
          eventName = ['change', attrName].join(':')

      model.on(eventName, function (_, newVal) {
        el[type](newVal)
      })
    })
  }

  // PlaylistView
  // @view
  //
  // represents a playlist model and provides an interface for editing, removing
  // and searching for tracks
  return backbone.View.extend({
    tagName: 'li',

    className: 'playlist',

    events: {
      'click .edit': 'startEditing',
      'submit form': 'updatePlaylist',
      'click .cancel': 'stopEditing',
      'click .delete': 'deletePlaylist',
      'change input[type="search"]': 'trackSearch'
    },

    initialize: function () {
      this.model.on('change', this.stopEditing, this)
      this.model.on('destroy', this.remove, this)
    },

    // serialize the form into an object and then update the model with these params
    updatePlaylist: function (e) {
      e.preventDefault()
      var params = $(e.target).serializeObject()
      this.model.save(params)
    },

    // confirm and then destroy the playlist
    deletePlaylist: function (e) {
      e.preventDefault()
      this.model.destroy()
    },

    startEditing: function () {
      this.$el.addClass('is-editing')
    },

    stopEditing: function (e) {
      if (e.preventDefault) e.preventDefault()
      this.$el.removeClass('is-editing')
      this.$el.find('form').get(0).reset()
    },

    // create a new search with the query term entered in the search box
    // render a TrackSearchResult with the results of the query
    // bind to track selection and add that track to this playlist
    trackSearch: function (e) {
      this.$el.find('.search-results').remove()

      var trackSearchResults = new TrackSearchResults,
          query = $(e.target).val()

      var trackSearchResultsView = new TrackSearchResultsView ({
        collection: trackSearchResults,
        el: $('<ul>', {'class': 'search-results'})
      })

      this.$el.find('.track-search').append(trackSearchResultsView.render())

      trackSearchResults.query(query)

      trackSearchResults.on('selected', function (track) {
        this.model.addTrack(track)
      }, this)
    },

    // render the template for the playlist and a list of the tracks that
    // make up this playlist
    render: function () {
      this.$el.html(template(this.model.toJSON()))

      new TracksView ({
        collection: this.model.tracks,
        el: this.$el.find('.tracks ul')
      }).render()

      autoBind(this.$el, this.model, 'val')
      autoBind(this.$el, this.model, 'text')
    }
  })
})
