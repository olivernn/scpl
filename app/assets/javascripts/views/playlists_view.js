define(['backbone', 'views/playlist_view'], function (backbone, PlaylistView) {
  // PlaylistsView
  // @view
  //
  // represents a list of playlists and handles rendering new
  // playlists when they get added.
  return backbone.View.extend({

    initialize: function () {
      this.collection.on('add', this.appendPlaylist, this)
    },

    appendPlaylist: function (playlist) {
      var playlistView = new PlaylistView ({
        model: playlist
      })

      playlistView.render()

      this.$el.append(playlistView.el)
    },

    render: function () {
      this.collection.forEach(this.appendPlaylist, this)
    }
  })
})
