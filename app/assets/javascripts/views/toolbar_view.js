define(['backbone'], function (backbone) {
  // ToolbarView
  // @view
  //
  // handles the small toolbar in the header
  return backbone.View.extend({
    events: {
      'click .new-playlist': 'addNewPlaylist'
    },

    addNewPlaylist: function () {
      this.collection.create()
    }
  })
})
