define(['backbone', 'template!player'], function (backbone, template) {
  // PlayerView
  // @view
  //
  // Represents a playing track, provides an interface for playing, pausing and skipping
  // playback on a track.  Also provides a representation of the playback progress.
  return backbone.View.extend({
    initialize: function () {
      this.model.on('playback:started playback:resumed', this.isPlaying, this)
      this.model.on('playback:finished playback:paused', this.notPlaying, this)
      this.model.on('playback:finished', this.remove, this)
      this.model.on('change:progress', this.updateProgress, this)
    },

    events: {
      'click .pause': 'pause',
      'click .play': 'play',
      'click .next': 'stop'
    },

    pause: function () {
      this.model.pausePlayback()
    },

    play: function () {
      this.model.resumePlayback()
    },

    stop: function () {
      this.model.stopPlayback()
    },

    isPlaying: function () {
      this.$el.find('.player').addClass('is-playing')
    },

    notPlaying: function () {
      this.$el.find('.player').removeClass('is-playing')
    },

    // display the progress of the playback of the current track
    updateProgress: function () {
      this.$el.find('.progress-inner').css('width', this.model.get('progress') + '%')
    },

    render: function () {
      this.$el.html(template(this.model.attributes))
      return this.$el
    }
  })
})
