define(['backbone', 'template!search_result'], function (backbone, template) {
  // TrackSearchResultView
  // @view
  //
  // represents a single track from a search result
  // handles the click on the track view.
  return backbone.View.extend({
    events: {
      'click': 'selectTrack'
    },

    selectTrack: function () {
      this.model.select()
    },

    render: function () {
      return this.$el.html(template(this.model.attributes))
    }
  })
})
