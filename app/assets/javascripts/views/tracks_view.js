define(['backbone', 'views/track_view', 'jquery'], function (backbone, TrackView, $) {
  // TrackView
  // @view
  //
  // a view representing a collection of tracks
  // takes care of rendering new tracks when they
  // are added to the tracks collection
  return backbone.View.extend({
    initialize: function () {
      this.collection.on('add', this.renderTrack, this)
    },

    renderTrack: function (track) {
      var trackView = new TrackView ({
        model: track,
        el: $('<li>')
      })

      this.$el.append(trackView.render())
    },

    render: function () {
      this.collection.forEach(this.renderTrack, this)
    }
  })
})
