define(['backbone', 'template!track'], function (backbone, template) {
  return backbone.View.extend({
    events: {
      'click nav .delete': 'deleteTrack',
      'click nav .play': 'playTrack',
      'click nav .pause': 'pauseTrack'
    },

    initialize: function () {
      this.model.on('destroy', this.remove, this)
      this.model.on('playback:started playback:resumed', this.isPlaying, this)
      this.model.on('playback:killed playback:finished playback:paused', this.notPlaying, this)
    },

    isPlaying: function () {
      this.$el.find('.track').addClass('is-playing')
    },

    notPlaying: function () {
      this.$el.find('.track').removeClass('is-playing')
    },

    playTrack: function (e) {
      e.preventDefault()
      e.stopPropagation()

      this.model.startPlayback()
    },

    pauseTrack: function (e) {
      e.preventDefault()
      e.stopPropagation()

      this.model.pausePlayback()
    },

    deleteTrack: function (e) {
      e.preventDefault()
      e.stopPropagation()

      this.model.destroy()
    },

    render: function () {
      return this.$el.html(template(this.model.attributes))
    }
  })
})
