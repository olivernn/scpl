define(['backbone', 'views/track_search_result_view', 'underscore'], function (backbone, TrackSearchResultView, _) {
  // TrackSearchResultsView
  // @view
  //
  // represents a list of tracks resulting from a track search
  // handles removing the search results when a track has been 
  // selected or when the esc key is hit
  return backbone.View.extend({
    initialize: function () {
      this.collection.on('searchComplete', this.render, this)
      this.collection.on('selected', this.remove, this)
      this.boundRemoveOnEsc = _.bind(this.removeOnEsc, this)
      $(document).on('keyup', this.boundRemoveOnEsc)
    },

    renderResult: function (result) {
      var trackSearchResultView = new TrackSearchResultView ({
        model: result,
        el: $('<li>')
      })

      this.$el.append(trackSearchResultView.render())

    },

    // binds to the keyup event and checks for the esc key
    // removes the view after the esc key is pressed.
    removeOnEsc: function (e) {
      if (e.keyCode === 27) {
        $(document).off('keyup', this.boundRemoveOnEsc)
        this.remove()
      }
    },

    render: function () {
      this.collection.forEach(this.renderResult, this)
      return this.$el
    }
  })
})
