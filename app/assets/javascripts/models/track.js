define(['backbone'], function (backbone) {
  // Track
  //
  // A single SoundCloud track, hanldles the playback of a track.
  return backbone.Model.extend({
    select: function () {
      this.trigger('selected', this)
    },

    // starts playback of the track using the SoundCloud SDK.
    // stores the sound object returned from the SDK and sets up the
    // listeners for progress and finish events.
    startPlayback: function () {
      var self = this,
          streamUrl = ['/tracks', this.get('sc_id')].join('/')

      if (this.has('sound') && this.get('progress') < 100) return this.resumePlayback()

      var soundOptions = {
        onfinish: function () {
          self.trigger('playback:finished', self)
        },

        whileplaying: function () {
          var progress = Math.round(this.position / this.duration * 100)
          self.set('progress', progress)
        }
      }

      SC.stream(streamUrl, soundOptions, function (sound) {
        self.set('sound', sound)
        sound.play()
        self.trigger('playback:started', self)
      })
    },

    // pauses the playback of the sound and triggers an event so everyone knows
    pausePlayback: function () {
      if (!this.has('sound')) return

      this.get('sound').pause()
      this.trigger('playback:paused', this)
    },

    // resumes playback and triggers an event so everyone can know
    resumePlayback: function () {
      if (!this.has('sound')) return

      this.get('sound').resume()
      this.trigger('playback:resumed', this)
    },

    // stops playback, also resets both the sound and progress attributes.
    stopPlayback: function () {
      if (!this.has('sound')) return

      this.get('sound').stop()
      if (trigger) this.trigger('playback:finished', this)

      this.set('sound', null)
      this.set('progress', 0)
    },

    // kills playback, very similar to the stopPlayback function except it doesnt
    // trigger any events
    killPlayback: function () {
      if (this.has('sound')) {
        this.get('sound').stop()
      }

      this.trigger('playback:killed')
      this.set('sound', null)
      this.set('progress', 0)
    },

    // custom json serialization
    toJSON: function () {
      return {
        title: this.get('title'),
        sc_id: this.get('sc_id')
      }
    }
  })
})
