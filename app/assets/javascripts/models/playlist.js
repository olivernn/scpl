define(['backbone', 'collections/tracks'], function (backbone, TracksCollection) {
  // Playlist
  // @model
  //
  // Represents a single playlist and manages the tracks and their playback.
  //
  return backbone.Model.extend({
    initialize: function () {
      var self = this

      // default attributes for new playlists
      if (!this.id) {
        this.set('title', 'New playlist')
        this.set('description', '')
      }

      // setup a collection to hold this playlists tracks
      this.tracks = new TracksCollection

      this.tracks.url = function () {
        return ['/playlists', self.id, 'tracks'].join('/')
      }

      // manage the playback on this playlists tracks
      // when a track playback starts make sure that no other track is playing and bubble the event upwards
      this.tracks.on('playback:started', function (track) {
        this.trigger('playback:started', track, this)
        this.tracks.forEach(function (t) { if (t.id !== track.id) t.killPlayback() })
      }, this)

      // when a playlist's track has finished playing, try and start the next one playing unless this
      // track was the last one.
      this.tracks.on('playback:finished', function (track) {
        var idx = this.tracks.indexOf(track),
            next = this.tracks.at(idx + 1)

        if (next) next.startPlayback()
      }, this)

      // populate the tracks collection if this playlist already has tracks
      if (!this.has('tracks')) return

      this.get('tracks').forEach(function (track) {
        this.tracks.add(track)
      }, this)
    },

    // stop playback on all the tracks in this playlist
    stopPlayback: function () {
      this.tracks.forEach(function (t) { t.killPlayback() })
    },

    // add a new track to this playlists colleciton of tracks
    addTrack: function (track) {
      this.tracks.create(track.toJSON())
    }
  })
})
