define(['backbone', 'models/playlist'], function (backbone, Playlist) {
  // Playlists
  // @collection
  //
  // handles all the playlists in the app
  return backbone.Collection.extend({
    model: Playlist,
    url: '/playlists'
  })
})
