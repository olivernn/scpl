define(['backbone', 'soundcloud', 'models/track'], function (backbone, sc, Track) {
  // TrackSearchResults
  // @collection
  //
  // Manages track searches afainst the SoundCloud api.
  // will hold a colleciton of tracks representing the search results from the soundcloud api.
  return backbone.Collection.extend({
    model: Track,

    // makes a query to the soundcloud api and then adds instances of tracks.
    // Once the search completes it triggers an event to let everyone know.
    query: function (query) {
      var renameId = function (track) {
        track.sc_id = track.id
        delete track.id
        return track
      }

      var resultsLoaded = function (tracks) {
        this.add(tracks.map(renameId))
        this.trigger('searchComplete')
      }

      sc.get('/tracks', {q: query}, resultsLoaded.bind(this))
    }
  })
})
