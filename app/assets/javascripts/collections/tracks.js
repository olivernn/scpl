define(['backbone', 'models/track'], function (backbone, Track) {
  // Tracks
  // @colleciton
  //
  // Handles a playlists tracks, each playlist holds an instance of this colleciotn
  return backbone.Collection.extend({
    model: Track
  })
})
