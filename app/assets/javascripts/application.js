require([
  'jquery',
  'collections/playlists',
  'views/playlists_view',
  'views/toolbar_view',
  'views/player_view'
], function ($, Playlists, PlaylistsView, ToolbarView, PlayerView) {
  $.ajaxPrefilter(function (_, _, xhr) {
    xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
  })

  var playlists = new Playlists
  playlists.reset(playlistJSON)

  var toolbarView = new ToolbarView ({
    collection: playlists,
    el: $('.toolbar')
  })

  toolbarView.render()

  var playlistsView = new PlaylistsView ({
    collection: playlists,
    el: $('.playlists')
  })

  playlists.on('playback:started', function (track, playlist) {
    playlists.forEach(function (p) { if (p.id !== playlist.id) p.stopPlayback() })

    var playerView = new PlayerView ({
      model: track
    })

    $('.player-container').html(playerView.render())
  })

  playlistsView.render()
})
