class PlaylistsController < ApplicationController
  respond_to :json

  def index
    @playlists = Playlist.all
  end

  def create
    @playlist = Playlist.create(playlist_params)
    respond_with @playlist
  end

  def update
    @playlist = Playlist.find(params[:id])
    respond_with @playlist.update_attributes(playlist_params)
  end

  def destroy
    @playlist = Playlist.find(params[:id])
    respond_with @playlist.destroy
  end

  private

  def playlist_params
    params.only("title", "description")
  end
end
