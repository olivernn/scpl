class TracksController < ApplicationController

  before_filter :load_playlist

  respond_to :json

  def create
    @track = @playlist.tracks.create(track_params)
    respond_with @track, location: nil
  end

  def destroy
    @track = @playlist.tracks.find(params[:id])
    respond_with @track.destroy, location: nil
  end

  def resolve
    response.headers['Access-Control-Allow-Origin'] = 'https://soundcloud.com'
    @playlist.tracks.create_from_url(params[:soundcloud_url])
    render nothing: true
  end

  private

  def load_playlist
    @playlist = Playlist.find(params[:playlist_id])
  end

  def track_params
    params.only("title", "sc_id")
  end
end
