module ApplicationHelper
  def hostname
    if Rails.env.development?
      "#{request.host}:#{request.port}"
    else
      request.host
    end
  end
end
