# Load the rails application
require File.expand_path('../application', __FILE__)
require File.expand_path('../../lib/core_extensions/hash.rb', __FILE__)

# Initialize the rails application
Scpl::Application.initialize!
