Scpl::Application.routes.draw do
  resources :playlists, only: [:index, :update, :create, :destroy] do
    resources :tracks, only: [:create, :destroy] do
      collection do
        post :resolve
      end
    end
  end

  resource :bookmarklet, only: :show

  root to: 'playlists#index'
end
